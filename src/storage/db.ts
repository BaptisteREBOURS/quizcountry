import { SQLite } from "@ionic-native/sqlite";
import { Partie } from "../model/Partie";
import { isPlatform } from "@ionic/react";
import defaultParties from "../model/historique/parties.json";

var inMemoryParties = defaultParties;

const initDBIfNeeded = async () => {
  const db = await SQLite.create({
    name: "data2.db",
    location: "default",
  });
  await db.executeSql(
    "CREATE TABLE IF NOT EXISTS parties(identifiant INTEGER PRIMARY KEY AUTOINCREMENT, mode TEXT, region TEXT, niveau TEXT, score INTEGER)",
    []
  );
  return db;
};

export const getParties = async () => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    return inMemoryParties;
  }

  const data = await (await initDBIfNeeded()).executeSql(
    "SELECT * FROM parties",
    []
  );
  const retour: Partie[] = [];
  for (let index = 0; index < data.rows.length; index++) {
    const element = data.rows.item(index);
    retour.unshift(element);
  }
  return retour;
};

export const addPartie = async (partie: Partie) => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    inMemoryParties.unshift(partie);
    return inMemoryParties;
  }

  await (
    await initDBIfNeeded()
  ).executeSql(
    "INSERT INTO parties(mode,region,niveau,score) VALUES(?,?,?,?)",
    [partie.mode, partie.region, partie.niveau, partie.score]
  );

  return getParties();
};
