export interface Partie {
  mode: string;
  region: string;
  niveau: string;
  score: number;
}
