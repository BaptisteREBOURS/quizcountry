import React from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import "./Tab2.css";
import { SelectionGame } from "../components/SelectionGame";

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Jeu</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Jeu</IonTitle>
          </IonToolbar>
        </IonHeader>
        <SelectionGame />
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
