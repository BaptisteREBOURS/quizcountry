import React from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import "./Tab1.css";
import { PageAccueil } from "../components/PageAccueil";

const Tab1: React.FC = () => {
  return (
    <IonContent class="tab1">
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Accueil</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Accueil</IonTitle>
            </IonToolbar>
          </IonHeader>
          <PageAccueil />
        </IonContent>
      </IonPage>
    </IonContent>
  );
};

export default Tab1;
