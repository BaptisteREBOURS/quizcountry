import React, { useState } from "react";
import {
  IonButton,
  IonList,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonImg,
  IonContent,
  IonGrid,
  IonCol,
  IonRow,
  IonIcon,
  IonInput,
} from "@ionic/react";
import "./PageAccueil.css";
import { earth, trophy, chevronBack } from "ionicons/icons";
import data from "../data/data-countries.json";

export const PageAccueil = () => {
  const [affichagePays, setAffichagePays] = useState(false);
  const [affichageNotice, setAffichageNotice] = useState(false);
  const [filtrePays, SetFiltrePays] = useState<string>("");
  const [filtreCapitale, SetFiltreCapitale] = useState<string>("");
  const [filtreContinent, SetFiltreContinent] = useState<string>("");

  return (
    <>
      {!affichagePays && !affichageNotice && (
        <IonContent>
          <h2 id="bienvenue">Bienvenue sur QuizCountry !</h2>
          <h4>Devenez incollable en géographie</h4>
          <IonImg src="../assets/icon/logo_appli.png" alt="" />
          <IonGrid>
            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol offset-4>
                <div>Notice d'utilisation de l'application</div>
              </IonCol>
              <IonCol>
                <div>Informations sur les pays</div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol>
                <IonButton
                  onClick={() => {
                    setAffichageNotice(true);
                  }}
                  fill="solid"
                  color="light"
                >
                  Notice
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  onClick={() => {
                    setAffichagePays(true);
                  }}
                  fill="solid"
                  color="light"
                >
                  Liste des pays
                </IonButton>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      )}
      {affichagePays && (
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol size="1">
                <IonIcon
                  onClick={() => {
                    setAffichagePays(false);
                  }}
                  icon={chevronBack}
                  class="flecheretour"
                ></IonIcon>
              </IonCol>
              <IonCol>
                <IonSelect
                  value={filtreContinent}
                  interface="popover"
                  onIonChange={(e) => SetFiltreContinent(e.detail.value)}
                >
                  <IonSelectOption value="">Monde</IonSelectOption>
                  <IonSelectOption value="Afrique">Afrique</IonSelectOption>
                  <IonSelectOption value="Amérique">Amérique</IonSelectOption>
                  <IonSelectOption value="Asie">Asie</IonSelectOption>
                  <IonSelectOption value="Europe">Europe</IonSelectOption>
                  <IonSelectOption value="Océanie">Océanie</IonSelectOption>
                </IonSelect>
              </IonCol>
              <IonCol>
                <IonInput
                  value={filtrePays}
                  placeholder="Pays"
                  onIonChange={(e: any) => SetFiltrePays(e.target.value)}
                ></IonInput>
              </IonCol>
              <IonCol>
                <IonInput
                  value={filtreCapitale}
                  placeholder="Capitale"
                  onIonChange={(e: any) => SetFiltreCapitale(e.target.value)}
                ></IonInput>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
            {(filtreContinent !== "" ||
              filtrePays !== "" ||
              filtreCapitale !== "") && (
              <IonRow>
                <IonCol>
                  <IonButton
                    onClick={() => {
                      SetFiltreContinent("");
                      SetFiltrePays("");
                      SetFiltreCapitale("");
                    }}
                    fill="solid"
                    color="light"
                  >
                    Réinitialiser
                  </IonButton>
                </IonCol>
              </IonRow>
            )}
          </IonGrid>
          <IonGrid>
            <IonRow>
              <IonCol size="3"></IonCol>
              <IonCol>
                <IonList>
                  {data.Countries.filter(
                    (country) =>
                      (!filtrePays ||
                        country.name
                          .toLowerCase()
                          .startsWith(filtrePays.toLowerCase())) &&
                      (!filtreCapitale ||
                        country.capital
                          .toLowerCase()
                          .startsWith(filtreCapitale.toLowerCase())) &&
                      (!filtreContinent || country.region === filtreContinent)
                  ).map((element) => (
                    <IonItem>
                      <IonImg id="drapeau" slot="start" src={element.flag} />
                      <IonLabel>
                        <h2 id="nom_pays">{element.name}</h2>
                        <h3>
                          {element.region} ({element.subregion})
                        </h3>
                        <h4 id="nom_capitale">
                          Capitale :{" "}
                          {element.capital === "" ? "Aucune" : element.capital}
                        </h4>
                      </IonLabel>
                    </IonItem>
                  ))}
                </IonList>
              </IonCol>
              <IonCol size="3"></IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      )}
      {affichageNotice && (
        <>
          <IonContent>
            <IonGrid>
              <IonRow>
                <IonCol size="0.5">
                  <IonIcon
                    onClick={() => {
                      setAffichageNotice(false);
                    }}
                    icon={chevronBack}
                    class="flecheretour"
                  ></IonIcon>
                </IonCol>
                <IonCol>
                  <h4>Notice d'utilisation de l'application</h4>
                </IonCol>
                <IonCol size="0.5"></IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <div>
                    Pour jouer, cliquez sur l'onglet <IonIcon icon={earth} />
                  </div>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <div>
                    Pour voir vos scores obtenus aux précédentes parties,
                    cliquez sur l'onglet <IonIcon icon={trophy} />
                  </div>
                </IonCol>
              </IonRow>
              <br />
              <IonRow>
                <IonCol>
                  <div id="justifie">
                    2 modes de jeu sont disponibles : Capitales et Drapeaux.
                    Vous pouvez également choisir la région du monde sur
                    laquelle vous voulez tester vos connaissances, ou bien le
                    niveau de jeu. Au début, seul le niveau Facile est
                    disponible. Pour débloquer les niveaux Moyen et Difficile,
                    un score de 10 aux niveaux Facile et Moyen sont
                    respectivement nécessaires.
                  </div>
                  <br />
                  <div id="justifie">
                    Pour chacun des modes, lorsque vous lancez une partie, 10
                    questions vous sont posées. Pour chacune d'entre elles, vous
                    avez 10 secondes pour trouver la bonne réponse.
                  </div>
                  <br />
                  <br />
                  <div id="justifie">
                    Dans l'onglet Scores, vous pouvez visualiser les scores que
                    vous avez obtenu aux précédentes parties. Le tableau est
                    interactif ; cliquez sur une colonne pour trier les valeurs
                    du tableau selon ce champ.
                  </div>
                  <br />
                  <div>Have fun !</div>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonContent>
        </>
      )}
    </>
  );
};
