import React, { useState, useEffect } from "react";
import "./CapitalGame.css";
import {
  IonButton,
  IonApp,
  IonContent,
  IonIcon,
  IonImg,
  IonGrid,
  IonRow,
  IonCol,
  IonAlert,
  IonFab,
  IonFabButton,
  IonFabList,
} from "@ionic/react";
import { SelectionGame } from "../components/SelectionGame";
import { addPartie, getParties } from "../storage/db";
import { Partie } from "../model/Partie";
import {
  checkmarkCircleOutline,
  closeCircleOutline,
  share,
  logoFacebook,
  logoTwitter,
  logoWhatsapp,
} from "ionicons/icons";
import { Vibration } from "@ionic-native/vibration";
import { isPlatform } from "@ionic/react";
import { SocialSharing } from "@ionic-native/social-sharing";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import { Media, MediaObject } from "@ionic-native/media";

const mediaBonneRep: MediaObject = Media.create(
  "cdvfile://localhost/assets/public/assets/audio/son_bonnerep.mp3"
);
const mediaMauvaiseRep: MediaObject = Media.create(
  "cdvfile://localhost/assets/public/assets/audio/son_mauvaiserep.mp3"
);
const mediaNivDebloque: MediaObject = Media.create(
  "cdvfile://localhost/assets/public/assets/audio/son_niveaudebloque.mp3"
);

interface Props {
  data: {
    name: string;
    capital: string;
    region: string;
    level: string;
  }[];
  filtre_region: string;
  filtre_niveau: string;
}

const messageShare = (score: number) => {
  var message =
    "J'ai obtenu un score de " + score + "/10 au mode de jeu Capitales !";
  return message.concat(" \n Envoyé depuis QuizCountry !");
};

const whatsappShare = (score: number) => {
  var message_share = messageShare(score);
  SocialSharing.shareViaWhatsApp(message_share);
};

const twitterShare = (score: number) => {
  var message_share = messageShare(score);
  SocialSharing.shareViaTwitter(message_share);
};

const facebookShare = (score: number) => {
  var message_share = messageShare(score);
  SocialSharing.shareViaFacebook(message_share);
};

const renderTime = (remainingTime: any) => {
  if (remainingTime === 0) {
    return <div className="timer">Trop tard</div>;
  }

  return (
    <div className="timer">
      <div className="value">{remainingTime}</div>
    </div>
  );
};

const RandomCountryCaps = (props: Props) => {
  let allRandomData: Array<[string, string, Array<string>]> = [];
  let pays_tires: Array<string> = [];

  for (var i = 0; i < 11; i++) {
    if (props.filtre_region !== "") {
      const dataFiltrees = props.data.filter(
        (country) =>
          !pays_tires.includes(country.name) &&
          country.region === props.filtre_region
      );

      const nb_pays = dataFiltrees.length;

      let arr: number[] = [];
      while (arr.length < 4) {
        const r = Math.floor(Math.random() * nb_pays);
        if (arr.indexOf(r) === -1) arr.push(r);
      }

      const pays: string = dataFiltrees[arr[0]].name;

      pays_tires.push(pays);

      const cap_true: string = dataFiltrees[arr[0]].capital;

      let caps: Array<string> = [];
      arr.map((element) => caps.push(dataFiltrees[element].capital));

      for (var position = caps.length - 1; position >= 1; position--) {
        var hasard = Math.floor(Math.random() * (position + 1));
        var sauve = caps[position];
        caps[position] = caps[hasard];
        caps[hasard] = sauve;
      }

      const randomData: [string, string, Array<string>] = [
        pays,
        cap_true,
        caps,
      ];
      allRandomData.push(randomData);
    } else {
      const dataFiltrees = props.data.filter(
        (country) =>
          !pays_tires.includes(country.name) &&
          country.level === props.filtre_niveau
      );

      const nb_pays = dataFiltrees.length;

      let arr: number[] = [];
      while (arr.length < 4) {
        const r = Math.floor(Math.random() * nb_pays);
        if (arr.indexOf(r) === -1) arr.push(r);
      }

      const pays: string = dataFiltrees[arr[0]].name;

      pays_tires.push(pays);

      const cap_true: string = dataFiltrees[arr[0]].capital;

      let caps: Array<string> = [];
      arr.map((element) => caps.push(dataFiltrees[element].capital));

      for (var position2 = caps.length - 1; position2 >= 1; position2--) {
        var hasard2 = Math.floor(Math.random() * (position2 + 1));
        var sauve2 = caps[position2];
        caps[position2] = caps[hasard2];
        caps[hasard2] = sauve2;
      }

      const randomData: [string, string, Array<string>] = [
        pays,
        cap_true,
        caps,
      ];
      allRandomData.push(randomData);
    }
  }

  return allRandomData;
};

export const CapitalGame = (props: Props) => {
  const [score_valeur, SetScore] = useState(0);
  const [iter_rep, SetIter] = useState(0);
  const [resrep, SetResRep] = useState("");

  const verifRep = (reponse: string, cap_true: string) => {
    if (reponse === cap_true) {
      SetScore(score_valeur + 1);
      SetResRep("correct");
      mediaBonneRep.play();
    } else {
      SetResRep("mauvais");
      if (!isPlatform("android") && !isPlatform("ios")) {
        mediaMauvaiseRep.play();
      } else {
        Vibration.vibrate(1000);
        mediaMauvaiseRep.play();
      }
    }
  };

  const [parties_avant, setPartiesAvant] = useState<Partie[]>([]);
  const [niveauMoyenCap, setNiveauMoyenCap] = useState(false);
  const [niveauDifCap, setNiveauDifCap] = useState(false);
  useEffect(() => {
    getParties().then((res) => setPartiesAvant(res));
  }, []);
  const [alerteCapMoyenDebloque, setAlerteCapMoyenDebloque] = useState(false);
  const [alerteCapDifficileDebloque, setAlerteCapDifficileDebloque] = useState(
    false
  );

  var ajouterPartieHistorique = (score_valeur: number) => {
    if (props.filtre_region === "") {
      var list_parties_avant: Array<[string, string]> = [];
      var moyen_debloque: Array<boolean> = [];
      var difficile_debloque: Array<boolean> = [];
      parties_avant.map((element) =>
        list_parties_avant.push([element.mode, element.niveau])
      );
      list_parties_avant.map((element) =>
        moyen_debloque.push(
          element[0] === "Capitales" && element[1] === "Moyen"
        )
      );
      list_parties_avant.map((element) =>
        difficile_debloque.push(
          element[0] === "Capitales" && element[1] === "Difficile"
        )
      );

      if (difficile_debloque.includes(true)) {
        setNiveauDifCap(true);
        setNiveauMoyenCap(true);
      } else if (moyen_debloque.includes(true)) {
        setNiveauMoyenCap(true);
      }

      const new_partie: Partie = {
        mode: "Capitales",
        region: "Monde",
        niveau: props.filtre_niveau,
        score: score_valeur,
      };

      if (
        score_valeur === 10 &&
        props.filtre_niveau === "Facile" &&
        niveauMoyenCap === false
      ) {
        setAlerteCapMoyenDebloque(true);
        mediaNivDebloque.play();
      } else if (
        score_valeur === 10 &&
        props.filtre_niveau === "Moyen" &&
        niveauDifCap === false
      ) {
        setAlerteCapDifficileDebloque(true);
        mediaNivDebloque.play();
      }

      addPartie(new_partie);
    } else {
      const new_partie: Partie = {
        mode: "Capitales",
        region: props.filtre_region,
        niveau: "Tout niveau",
        score: score_valeur,
      };
      addPartie(new_partie);
    }
  };

  const [allRandomData, setAllRandomData] = useState(RandomCountryCaps(props));
  const [dis_start, setDisStart] = useState(false);
  const [selectionAffichage, setSelectionAffichage] = useState(false);
  const [resrepAffichage, SetResrepAffichage] = useState(false);
  const [dis_rep, SetDisRep] = useState(false);
  const [dis_next, SetDisNext] = useState(true);
  const [bonneRep, SetBonneRep] = useState("");
  const [timer, SetTimer] = useState(true);
  const [alerteAbandon, setAlerteAbandon] = useState(false);

  if (iter_rep < 11) {
    return (
      <IonContent class="div_all">
        {!dis_start && !selectionAffichage && (
          <IonContent>
            <h5 id="title">Options de la partie</h5>
            <div>
              <div>Mode de jeu : Capitales</div>
              <div>
                Région :{" "}
                {props.filtre_region === "" ? "Monde" : props.filtre_region}
              </div>
              <div>
                Niveau :{" "}
                {props.filtre_niveau === ""
                  ? "Tout niveau"
                  : props.filtre_niveau}
              </div>
            </div>
            <br />
            <IonButton
              onClick={() => {
                SetScore(0);
                SetIter(1);
                setDisStart(true);
              }}
              fill="solid"
              color="light"
            >
              Lancer une partie
            </IonButton>
            <br />
            <IonButton
              onClick={() => {
                setSelectionAffichage(true);
              }}
              fill="solid"
              color="light"
            >
              Changer les options
            </IonButton>
          </IonContent>
        )}
        {selectionAffichage && (
          <>
            <SelectionGame />
            <IonAlert
              isOpen={alerteAbandon}
              onDidDismiss={() => setAlerteAbandon(false)}
              header={"BOUUUUUH"}
              subHeader={"Loser*"}
              message={
                "*Loser = Un loser est un perdant. Il s'agit d'un anglicisme péjoratif qualifiant une personne manquant d'assurance et enchaînant les mauvais choix ou coups du sort. A l'inverse du winner, le loser est un minable, un raté."
              }
              buttons={["OK"]}
              id="alerteAbandon"
            />
          </>
        )}
        {dis_start && iter_rep < 10 && (
          <IonApp>
            <IonContent>
              <IonGrid>
                <IonRow>
                  <IonCol size="2">
                    {timer && (
                      <CountdownCircleTimer
                        isPlaying
                        size={40}
                        duration={10}
                        colors={[
                          ["#004777", 0.25],
                          ["#F7B801", 0.25],
                          ["#A30000", 0.25],
                        ]}
                        onComplete={() => {
                          SetDisRep(true);
                          SetDisNext(false);
                          SetResrepAffichage(true);
                          SetBonneRep(allRandomData[iter_rep][1]);
                          SetResRep("mauvais");
                          SetTimer(false);
                        }}
                      >
                        {({ remainingTime }) => renderTime(remainingTime)}
                      </CountdownCircleTimer>
                    )}
                  </IonCol>
                  <IonCol></IonCol>
                  <IonCol size="2">{iter_rep}/10</IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <div>
                      <h2 id="label_pays">{allRandomData[iter_rep][0]}</h2>
                    </div>
                    {!resrepAffichage && (
                      <h5>Sélectionnez la bonne capitale</h5>
                    )}
                    {resrepAffichage && resrep === "correct" && (
                      <h5 id="bonnerep">
                        Bonne réponse{" "}
                        <IonIcon
                          id="iconbonnerep"
                          icon={checkmarkCircleOutline}
                        ></IonIcon>
                      </h5>
                    )}
                    {resrepAffichage && resrep === "mauvais" && (
                      <h5 id="mauvaiserep">
                        Mauvaise réponse{" "}
                        <IonIcon
                          id="iconmauvaiserep"
                          icon={closeCircleOutline}
                        ></IonIcon>{" "}
                        <br /> La bonne réponse était {bonneRep}
                      </h5>
                    )}
                    <br />
                    {allRandomData[iter_rep][2].map((element: string) => (
                      <IonButton
                        onClick={() => {
                          verifRep(element, allRandomData[iter_rep][1]);
                          SetDisNext(false);
                          SetDisRep(true);
                          SetResrepAffichage(true);
                          SetBonneRep(allRandomData[iter_rep][1]);
                          SetTimer(false);
                        }}
                        fill="solid"
                        color="light"
                        expand="block"
                        disabled={dis_rep}
                      >
                        {element}
                      </IonButton>
                    ))}
                    <br />
                    <IonButton
                      onClick={() => {
                        SetIter(iter_rep + 1);
                        SetDisNext(true);
                        SetDisRep(false);
                        SetResrepAffichage(false);
                        SetTimer(true);
                      }}
                      fill="solid"
                      color="light"
                      disabled={dis_next}
                    >
                      Pays suivant
                    </IonButton>
                    <IonButton
                      onClick={() => {
                        setDisStart(false);
                        setSelectionAffichage(true);
                        setAlerteAbandon(true);
                      }}
                      fill="solid"
                      color="light"
                    >
                      Abandonner
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonContent>
          </IonApp>
        )}
        {dis_start && iter_rep === 10 && (
          <IonApp>
            <IonContent class="div_all">
              <IonGrid>
                <IonRow>
                  <IonCol size="2">
                    {timer && (
                      <CountdownCircleTimer
                        isPlaying
                        size={40}
                        duration={10}
                        colors={[
                          ["#004777", 0.25],
                          ["#F7B801", 0.25],
                          ["#A30000", 0.25],
                        ]}
                        onComplete={() => {
                          SetDisRep(true);
                          SetDisNext(false);
                          SetResrepAffichage(true);
                          SetBonneRep(allRandomData[iter_rep][1]);
                          SetResRep("mauvais");
                          SetTimer(false);
                        }}
                      >
                        {({ remainingTime }) => renderTime(remainingTime)}
                      </CountdownCircleTimer>
                    )}
                  </IonCol>
                  <IonCol></IonCol>
                  <IonCol size="2">{iter_rep}/10</IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <div>
                      <h2 id="label_pays">{allRandomData[iter_rep][0]}</h2>
                    </div>
                    {!resrepAffichage && (
                      <h5>Sélectionnez la bonne capitale</h5>
                    )}
                    {resrepAffichage && resrep === "correct" && (
                      <h5 id="bonnerep">
                        Bonne réponse{" "}
                        <IonIcon
                          id="iconbonnerep"
                          icon={checkmarkCircleOutline}
                        ></IonIcon>
                      </h5>
                    )}
                    {resrepAffichage && resrep === "mauvais" && (
                      <h5 id="mauvaiserep">
                        Mauvaise réponse{" "}
                        <IonIcon
                          id="iconmauvaiserep"
                          icon={closeCircleOutline}
                        ></IonIcon>{" "}
                        <br /> La bonne réponse était {bonneRep}
                      </h5>
                    )}
                    <br />
                    {allRandomData[iter_rep][2].map((element: string) => (
                      <IonButton
                        onClick={() => {
                          verifRep(element, allRandomData[iter_rep][1]);
                          SetDisNext(false);
                          SetDisRep(true);
                          SetResrepAffichage(true);
                          SetBonneRep(allRandomData[iter_rep][1]);
                          SetTimer(false);
                        }}
                        fill="solid"
                        color="light"
                        expand="block"
                        disabled={dis_rep}
                      >
                        {element}
                      </IonButton>
                    ))}
                    <br />
                    <IonButton
                      onClick={() => {
                        SetIter(iter_rep + 1);
                        SetDisNext(true);
                        SetDisRep(false);
                        SetResrepAffichage(false);
                        ajouterPartieHistorique(score_valeur);
                      }}
                      fill="solid"
                      color="light"
                      disabled={dis_next}
                    >
                      Voir le score
                    </IonButton>
                    <IonButton
                      onClick={() => {
                        setDisStart(false);
                        setSelectionAffichage(true);
                        setAlerteAbandon(true);
                      }}
                      fill="solid"
                      color="light"
                    >
                      Abandonner
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonContent>
          </IonApp>
        )}
      </IonContent>
    );
  } else {
    return (
      <IonContent>
        {!selectionAffichage && (
          <>
            <IonContent>
              <IonFab vertical="top" horizontal="end" slot="fixed">
                <IonFabButton>
                  <IonIcon icon={share} />
                </IonFabButton>
                <IonFabList>
                  <IonFabButton onClick={() => whatsappShare(score_valeur)}>
                    <IonIcon icon={logoWhatsapp} />
                  </IonFabButton>
                  <IonFabButton onClick={() => facebookShare(score_valeur)}>
                    <IonIcon icon={logoFacebook} />
                  </IonFabButton>
                  <IonFabButton onClick={() => twitterShare(score_valeur)}>
                    <IonIcon icon={logoTwitter} />
                  </IonFabButton>
                </IonFabList>
              </IonFab>
              <div>
                <a>{score_valeur}</a> / 10
              </div>
              <IonGrid>
                <IonRow>
                  <IonCol size="2"></IonCol>
                  <IonCol>
                    {score_valeur === 0 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://media.giphy.com/media/wgmWyQGzaZ6Fy/giphy.gif"
                        />
                      </div>
                    )}
                    {score_valeur > 0 && score_valeur < 3 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://thumbs.gfycat.com/ScientificPointedHarborseal-size_restricted.gif"
                        />
                      </div>
                    )}
                    {score_valeur > 2 && score_valeur < 5 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://media.giphy.com/media/l2Je5Oz7LZ6Up9Dl6/giphy.gif"
                        />
                      </div>
                    )}
                    {score_valeur > 4 && score_valeur < 9 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://media.giphy.com/media/XBXBWRWbSmM6HnjErP/giphy.gif"
                        />
                      </div>
                    )}
                    {score_valeur > 8 && score_valeur < 10 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://media.giphy.com/media/XreQmk7ETCak0/giphy.gif"
                        />
                      </div>
                    )}
                    {score_valeur === 10 && (
                      <div>
                        <IonImg
                          id="gif_score"
                          src="https://media.giphy.com/media/g9582DNuQppxC/giphy.gif"
                        />
                      </div>
                    )}
                  </IonCol>
                  <IonCol size="2"></IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButton
                      onClick={() => {
                        setSelectionAffichage(true);
                      }}
                      fill="solid"
                      color="light"
                      id="bouton_newpartie"
                    >
                      {" "}
                      Nouvelle partie{" "}
                    </IonButton>
                    <IonAlert
                      isOpen={alerteCapMoyenDebloque}
                      onDidDismiss={() => setAlerteCapMoyenDebloque(false)}
                      header={"NIVEAU MOYEN DEBLOQUE"}
                      subHeader={"Mode de jeu Capitales"}
                      message={
                        "Vous pouvez désormais jouer au mode de jeu Capitales au niveau Moyen."
                      }
                      buttons={["OK"]}
                    />
                    <IonAlert
                      isOpen={alerteCapDifficileDebloque}
                      onDidDismiss={() => setAlerteCapDifficileDebloque(false)}
                      header={"NIVEAU DIFFICILE DEBLOQUE"}
                      subHeader={"Mode de jeu Capitales"}
                      message={
                        "Vous pouvez désormais jouer au mode de jeu Capitales au niveau Difficile."
                      }
                      buttons={["OK"]}
                    />
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonContent>
          </>
        )}
        {selectionAffichage && <SelectionGame />}
      </IonContent>
    );
  }
};
