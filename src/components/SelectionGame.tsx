import React, { useState, useEffect } from "react";
import {
  IonList,
  IonListHeader,
  IonItem,
  IonSelect,
  IonSelectOption,
  IonButton,
  IonContent,
  IonLabel,
  IonGrid,
  IonRow,
  IonCol,
} from "@ionic/react";
import { CapitalGame } from "../components/CapitalGame";
import data from "../data/data-countries.json";
import { DrapeauGame } from "./DrapeauGame";
import { getParties } from "../storage/db";
import { Partie } from "../model/Partie";

export const SelectionGame = () => {
  const [selectionAffichage, setSelectionAffichage] = useState(true);
  const [capitalAffichage, setCapitalAffichage] = useState(false);
  const [drapeauAffichage, setDrapeauAffichage] = useState(false);

  const [parties, setParties] = useState<Partie[]>([]);
  useEffect(() => {
    getParties().then((res) => setParties(res));
  }, []);

  var isDebloque = (mode_debloque: string, niveau_debloque: string) => {
    var list_parties: Array<[string, string, number]> = [];
    var result_debloque: Array<boolean> = [];
    parties.map((element) =>
      list_parties.push([element.mode, element.niveau, element.score])
    );
    list_parties.map((element) =>
      result_debloque.push(
        element[0] === mode_debloque &&
          element[1] === niveau_debloque &&
          element[2] === 10
      )
    );
    return result_debloque.includes(true);
  };

  var whichGame = (mode: string, region: string, niveau: string) => {
    if (region !== "Monde") {
      setNiveau("");
    } else {
      setRegion("");
    }
    if (mode === "Capitales") {
      setSelectionAffichage(false);
      setCapitalAffichage(true);
    } else if (mode === "Drapeaux") {
      setSelectionAffichage(false);
      setDrapeauAffichage(true);
    }
  };

  const [mode, setMode] = useState<string>("Capitales");
  const [region, setRegion] = useState<string>("Monde");
  const [niveau, setNiveau] = useState<string>("Facile");

  return (
    <>
      <IonContent>
        {selectionAffichage && (
          <>
            <IonGrid>
              <IonRow>
                <IonCol size="0.5"></IonCol>
                <IonCol>
                  <IonList>
                    <IonListHeader>
                      <IonLabel>
                        Sélectionnez le mode de jeu, la région et/ou le niveau
                        de difficulté
                      </IonLabel>
                    </IonListHeader>
                    <IonItem>
                      <IonLabel>Mode</IonLabel>
                      <IonSelect
                        value={mode}
                        onIonChange={(e) => setMode(e.detail.value)}
                        interface="popover"
                      >
                        <IonSelectOption value="Capitales">
                          Capitales
                        </IonSelectOption>
                        <IonSelectOption value="Drapeaux">
                          Drapeaux
                        </IonSelectOption>
                      </IonSelect>
                    </IonItem>
                    <IonItem>
                      <IonLabel>Région</IonLabel>
                      <IonSelect
                        value={region}
                        onIonChange={(e) => setRegion(e.detail.value)}
                        interface="popover"
                      >
                        <IonSelectOption value="Monde">Monde</IonSelectOption>
                        <IonSelectOption value="Afrique">
                          Afrique
                        </IonSelectOption>
                        <IonSelectOption value="Amérique">
                          Amérique
                        </IonSelectOption>
                        <IonSelectOption value="Asie">Asie</IonSelectOption>
                        <IonSelectOption value="Europe">Europe</IonSelectOption>
                        <IonSelectOption value="Océanie">
                          Océanie
                        </IonSelectOption>
                      </IonSelect>
                    </IonItem>
                    {region === "Monde" && (
                      <IonItem>
                        <IonLabel>Niveau de difficulté</IonLabel>
                        <IonSelect
                          value={niveau}
                          onIonChange={(e) => setNiveau(e.detail.value)}
                          interface="popover"
                        >
                          <IonSelectOption value="Facile">
                            Facile
                          </IonSelectOption>
                          {((mode === "Capitales" &&
                            isDebloque("Capitales", "Facile")) ||
                            (mode === "Drapeaux" &&
                              isDebloque("Drapeaux", "Facile"))) && (
                            <IonSelectOption value="Moyen">
                              Moyen
                            </IonSelectOption>
                          )}
                          {((mode === "Capitales" &&
                            isDebloque("Capitales", "Moyen")) ||
                            (mode === "Drapeaux" &&
                              isDebloque("Drapeaux", "Moyen"))) && (
                            <IonSelectOption value="Difficile">
                              Difficile
                            </IonSelectOption>
                          )}
                        </IonSelect>
                      </IonItem>
                    )}
                  </IonList>
                </IonCol>
                <IonCol size="0.5"></IonCol>
              </IonRow>
            </IonGrid>
            <IonButton
              onClick={() => {
                whichGame(mode, region, niveau);
              }}
              fill="solid"
              color="light"
            >
              Jouer
            </IonButton>
          </>
        )}
        {capitalAffichage && (
          <>
            <CapitalGame
              data={data.Countries}
              filtre_region={region}
              filtre_niveau={niveau}
            />
          </>
        )}
        {drapeauAffichage && (
          <>
            <DrapeauGame
              data={data.Countries}
              filtre_region={region}
              filtre_niveau={niveau}
            />
          </>
        )}
      </IonContent>
    </>
  );
};
