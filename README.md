# QuizCountry

Application mobile développée avec Ionic dans le cadre du cours optionnel de Technologies mobiles.

## Testez vos connaissances en géographie

Avec QuizCountry, testez vos connaissances sur les capitales et drapeaux des pays du monde entier. Devenez incollable grâce aux 2 modes de jeu disponibles pour le moment (Capitales et Drapeaux).

### Modes de jeu

Pour chacun de ces 2 modes, vous pouvez soit jouer sur les pays du monde entier, ou bien vous entraîner sur un continent en particulier. Vous pouvez aussi régler le niveau de difficulté (Facile, Moyen, Difficile). Au départ, seul le premier niveau est disponible. Pour débloquer les autres, obtenez un score de 10/10.

### Entrainement

Vous voulez connaître la capitale du Zimbabwe et vous n'avez pas internet pour le googler ? Vous ne vous sentez pas encore prêt à tenter le mode Difficile ? Révisez les capitales et les drapeaux en cliquant sur le bouton "Liste des pays" de la page d'accueil.

### Historique des parties

A tout moment, consultez l'historique de vos parties dans le troisième onglet de l'application.

### Partage du score

Vous avez obtenu le score parfait à une partie et vous voulez impressionner vos amis ? Vous pouvez le faire en sélectionnant l'application (Twitter, Facebook, Whatsapp) à la fin d'une partie.

## Installer l'application

1. Clonez le dépôt git dans le répertoire de votre choix
2. Exécutez les commandes suivantes

```
npm install
ionic serve
```

3. Enjoy

## Auteur

Baptiste REBOURS, 2A ENSAI

### Contact

baptiste.rebours@eleve.ensai.fr
